ALTER TABLE `fs_foodsaver` CHANGE `stat_fetchweight` `stat_fetchweight` DECIMAL(9,2) UNSIGNED NOT NULL DEFAULT '0.00';
ALTER TABLE `fs_foodsaver_archive` CHANGE `stat_fetchweight` `stat_fetchweight` DECIMAL(9,2) UNSIGNED NOT NULL DEFAULT '0.00';
ALTER TABLE `fs_foodsaver_archive2` CHANGE `stat_fetchweight` `stat_fetchweight` DECIMAL(9,2) UNSIGNED NOT NULL DEFAULT '0.00';
ALTER TABLE `fs_foodsaver_archive4` CHANGE `stat_fetchweight` `stat_fetchweight` DECIMAL(9,2) UNSIGNED NOT NULL DEFAULT '0.00';
ALTER TABLE `fs_foodsaver_archive5` CHANGE `stat_fetchweight` `stat_fetchweight` DECIMAL(9,2) UNSIGNED NOT NULL DEFAULT '0.00';
